Business Name: Leah Legal Criminal Defense

Address: 14401 Sylvan St. Suite #201, Van Nuys, California 91401 USA

Phone: (818) 484-1100

Website: https://www.leahlegal.com

Description: The criminal defense law firm of LeahLegal is dedicated to defending individuals who have been charged with or are under investigation for the commission of misdemeanor and felony crimes. We advocate for individuals of all ages, from all walks of life and at all stages of criminal proceedings.

Keywords: Criminal Defense Attorney, DUI defense, Criminal justice attorney.

Hour: 24/7.

Year: 2014.

Social Links:

https://www.facebook.com/leahcriminaldefense

https://twitter.com/LeahCrimDefense


